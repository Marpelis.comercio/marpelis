<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Articulo
 *
 * @author Marcos
 */

class Articulo extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper(['url']);
        $this->load->model('marpelis');
        $this->load->library('ion_auth'); //cargamos la librería
        if($this->ion_auth->logged_in()===FALSE)  //Si no estamos autenticados que nos reenvíe a la página de login
        {
          redirect('auth/login');
        }
    }
    
    public function inicio(){
        $datos['titulo'] = 'Marpelis';
        $this->load->view('commons/main_header',$datos);
        $data['resultado'] = $this->marpelis->get_articulos();
        $this->load->view('peliculas/lista',$data);
        $this->load->view('commons/main_footer');
    }
    public function generos(){
        $datos['titulo'] = 'Géneros';
        $this->load->view('commons/main_header',$datos);
        $generos = $this->marpelis->get_genero();
        $secciones = '';
        foreach ($generos as $genero){
            $data['peliculas']= $this->marpelis->get_pelicula_genero($genero->id);
            $data['nombre_genero']= $genero->nombre;
            $secciones.= $this->load->view('peliculas/genero',$data,TRUE);
            /*echo '<pre>';
            print_r($data);
            echo '</pre>';
            */
        }
        /*echo $secciones;*/
        $data['secciones'] = $secciones;
        $this->load->view('peliculas/generos',$data);
        $this->load->view('commons/main_footer');
    }
    public function pelicula($id){
        $datos['titulo'] = 'Pelicula';
        $this->load->view('commons/main_header',$datos);
        $data['resultado'] = $this->marpelis->get_pelicula($id);
        $data['genero'] = $this->marpelis->get_genero($data['resultado']->genero);
        $this->load->view('peliculas/entorno_peliculas',$data);
        $this->load->view('commons/main_footer');
    }
    public function administrator(){
        $datos['titulo'] = 'Administración de Películas';
        $this->load->view('commons/header',$datos);
        $data['resultado'] = $this->marpelis->get_articulos();
        $this->load->view('peliculas/tabla',$data);
        $this->load->view('commons/footer');
    }
    public function alta(){
        $this->load->helper('form');
        //primer paso
        $this->load->library(['form_validation']);
        //establecer las reglas
        $this->form_validation->set_rules('codigo', 'código de la película', 'required|is_unique[peliculas.codigo]|integer|trim');
        $this->form_validation->set_rules('nombre', 'nombre de la película', 'required|trim');
        $this->form_validation->set_rules('duracion', 'duración de la película', 'required|integer');
        $this->form_validation->set_rules('año', 'año de la película', 'required|integer');
        $this->form_validation->set_rules('genero', 'género de la película', 'required|is_natural_no_zero');
        $this->form_validation->set_rules('descripcion', 'descripción de la película', 'required');
        $this->form_validation->set_rules('youtube', 'link del video', 'required');
//diferencia entre el "bien" y el "mal"        
        if($this->form_validation->run()===FALSE){
            $datos['titulo'] = 'Alta de Nuevas películas';
            //obtener datos de generos
            $datos['generos'] = $this->marpelis->get_generos();
            $this->load->view('commons/header',$datos);
            $this->load->view('peliculas/ficha_alta');
        } else {
            $pelicula = [
               'nombre' => $this->input->post('nombre'),
                'codigo' => $this->input->post('codigo'),
                'duracion' => $this->input->post('duracion'),
                'año' => $this->input->post('año'),
                'genero' => $this->input->post('genero'),
                'descripcion' => $this->input->post('descripcion'),
                'youtube' => $this->input->post('youtube'),
            ];
            $config['upload_path'] = 'assets/images/articles';
            $config['file_name']   = $pelicula['codigo'].'.jpg';     
            $config['allowed_types'] = 'jpg';
            $config['max_size']      = 300; //kb
            $config['max_width']     = 1200;
            $config['max_height']    = 1600; //3x4
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('imagen'))
            //la subida sea correcta
            {    
                $this->marpelis->guardar($pelicula);
                $this->session->set_flashdata('exito', "<div class='bg-success'>Película: <strong>".$this->input->post('nombre')."</strong> añadido correctamente</div>");
                redirect(site_url('articulo/alta'));
            }
            else
            //error en la subida
            {
                $datos['error'] = "Error: ".$this->upload->display_errors(); 
                $datos['titulo'] = 'Alta de Nuevas Películas: Error';
                //obtener datos de categorias
                $datos['generos'] = $this->marpelis->get_generos();
                $this->load->view('commons/header',$datos);
                $this->load->view('peliculas/ficha_alta', $datos);
               
            }    
            $this->load->view('commons/footer');
        }
    }
    public function edita($id){
        $this->load->helper('form');
        //primer paso
        $this->load->library(['form_validation']);
        //establecer las reglas
        $this->form_validation->set_rules('codigo', 'código de la película', 'required|integer|trim');
        $this->form_validation->set_rules('nombre', 'nombre de la película', 'required|trim');
        $this->form_validation->set_rules('duracion', 'duración de la película', 'required|integer');
        $this->form_validation->set_rules('año', 'año de la película', 'required|integer');
        $this->form_validation->set_rules('genero', 'género de la película', 'required|is_natural_no_zero');
        $this->form_validation->set_rules('descripcion', 'descripción de la película', 'required');
        $this->form_validation->set_rules('youtube', 'link del video', 'required');
        
//diferencia entre el "bien" y el "mal"        
        if($this->form_validation->run()===FALSE){
            $datos['titulo'] = 'Modificación de Películas';
            //obtener datos de categorias
            $datos['generos'] = $this->marpelis->get_generos();
            //obtener los valores del artículo a modificar
            $datos['pelicula'] = $this->marpelis->get_pelicula($id);
            $datos['id'] = $id;
            $this->load->view('commons/header',$datos);
            $this->load->view('peliculas/ficha_edita');
            $this->load->view('commons/footer');
        } 
        else {
            $datos['titulo'] = 'Éxito: Película guardada';
            $this->load->view('commons/header',$datos);
            $pelicula = [
                'nombre' => $this->input->post('nombre'),
                'codigo' => $this->input->post('codigo'),
                'duracion' => $this->input->post('duracion'),
                'año' => $this->input->post('año'),
                'genero' => $this->input->post('genero'),
                'descripcion' => $this->input->post('descripcion'),
                'youtube' => $this->input->post('youtube'),
            ];
            $this->marpelis->actualizar($pelicula,$id);
            $this->load->view('commons/footer');
        }
        
    }
     public function borra($id){
        $this->marpelis->delete($id);
        echo "<h1>La Pelicula ha sido borrada</h1>";
        redirect(site_url('articulo/administrator'));
    }
    public function email(){
        $this->email->from('marpelis.comercio@yahoo.com');
        $this->email->to('jbarrachina@ausiasmarch.net');
        $this->email->subject('Prueba de envio de un correo electrónico');
        $this->email->message('<p><strong>Correo electrónico usando del smtp de gmail</strong></p><p> Enhorabuena si lees esto</p>');
        $this->email->send();
        //con esto podemos ver el resultado
        var_dump($this->email->print_debugger());
        //redirect('pedido');
    }
    

}

