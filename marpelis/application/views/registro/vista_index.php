<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.css');?>" />
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Bevan" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/DataTables-1.10.20/css/dataTables.bootstrap4.min.css');?>"/>
        <script type="text/javascript" src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/DataTables-1.10.20/js/jquery.dataTables.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/DataTables-1.10.20/js/dataTables.bootstrap4.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/tienda.js');?>"></script>
        <title>Inicio de Sesión peliculas Marpelis</title>
        <link  rel="icon" href="/assets/images/favicon.ico"/>
    </head>
    <body background="/assets/images/3.jpg">
        <div class="container-fluid">
            <div class="row" style="background-color: #212F3D;">
                <div class="col-1">
                    <img src="/assets/images/logo_blanco.png" class="img-responsive m-1 ml-5" style="width: 65%">
                </div>
                <div class="col-3">
                    <h1 class="text-white text-uppercase pl-4 mt-2" style="font-family: Bevan">
                    Marpelis
                    </h1>
                </div>
                <div class="col-2">
                    <a href="https://www.marvel.com/movies" target="_blank">
                    <img src="/assets/images/partner1.JPG" class="img-responsive mt-2 ml-5" style="width: 95%">
                    </a>
                </div>
                <div class="col-2">
                    <a href="https://www.dccomics.com/movies" target="_blank">
                    <img src="/assets/images/partner2.png" class="img-responsive mt-2 ml-4" style="width: 22%">
                    </a>
                </div>
                <div class="col-4">
                    <a class="float-right m-2 p-1 btn btn-light" href="https://gitlab.com/Marpelis.comercio/marpelis" role="button" target="_blank">
                        <span class="fab fa-gitlab fa-2x" style="color: #D35400" aria-hidden="true"></span>
                    </a> 
                    <a class="float-right m-2 p-1 btn btn-light" href="https://twitter.com/Marpelis1" role="button" target="_blank">
                        <span class="fab fa-twitter fa-2x" style="color: #00acee" aria-hidden="true"></span>
                    </a>                                      
                    <a class="float-right m-2 p-1 btn btn-light" href="https://www.facebook.com/profile.php?id=100051649306287" role="button" target="_blank">
                        <span class="fab fa-facebook-square fa-2x" style="color: #3b5998" aria-hidden="true"></span>
                    </a>                                     
                    <a class="float-right m-2 p-1 btn btn-light" href="https://www.instagram.com/marpelis.comercio/" role="button" target="_blank">
                        <span class="fab fa-instagram fa-2x" style="color: #C13584" aria-hidden="true"></span>
                    </a> 
                    
                </div>
            </div>
            <div class="row">
                <div class="col-3"></div>
                <div class="col-9">    
                  <div class="text-center text-white m-5 border border-light" style="width: 50%;background-color: #212F3D;">
                <?php
                echo isset($_SESSION['auth_message']) ? $_SESSION['auth_message'] : FALSE;
                ?>
                <h1 class="mt-3" style="font-family: Bevan;">Registro</h1>
                <?php
                echo form_open();
                echo form_label('Nombre:','first_name').'<br />';
                echo form_error('first_name');
                echo form_input('first_name',set_value('first_name')).'<br />';
                echo form_label('Apellidos:','last_name').'<br />';
                echo form_error('last_name');
                echo form_input('last_name',set_value('last_name')).'<br />';
                echo form_label('Email:','email').'<br />';
                echo form_error('email');
                echo form_input('email',set_value('email')).'<br />';
                echo form_label('Contraseña:', 'password').'<br />';
                echo form_error('password');
                echo form_password('password').'<br />';
                echo form_label('Comfirmar contraseña:', 'confirm_password').'<br />';
                echo form_error('confirm_password');
                echo form_password('confirm_password').'<br /><br />';
                echo form_submit('register','Register');
                echo form_close();
                ?>
                <?php $randomnumber = rand(1000, 9999); ?>
                <input name="active" id="active" type="hidden" value="false">
                <?php echo "<input name='code' id='code' value='".$randomnumber."' type='hidden'  required>"; ?>

            </div>
          </div>
         </div>
       