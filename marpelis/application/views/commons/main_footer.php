<footer class="page-footer pt-4">


<div class="container-fluid ">
      <div class="p-4 mx-0 border border-secondary" style="background-color: #1D2935">
        <div class="row">
            <div class="col-6">
                <a class="float-right ml-3 mb-4" href="https://twitter.com/Makros_7" role="button" target="_blank">
                    <span class="fab fa-twitter fa-2x" style="color: white" aria-hidden="true"></span>
                </a>                                      
                <a class="float-right mr-3 mb-4" href="https://www.facebook.com/marcos.martinezguardiola" role="button" target="_blank">
                    <span class="fab fa-facebook-square fa-2x" style="color: white" aria-hidden="true"></span>
                </a>  
            </div>
            <div class="col-6">
                <a class="float-left mr-3 mb-4" href="https://www.instagram.com/marcosmartinez.7/?hl=es" role="button" target="_blank">
                    <span class="fab fa-instagram fa-2x" style="color: white" aria-hidden="true"></span>
                </a>
                <a class="float-left ml-3 mb-4" href="https://gitlab.com/Marpelis.comercio/marpelis" role="button" target="_blank">
                    <span class="fab fa-gitlab fa-2x" style="color: white" aria-hidden="true"></span>
                </a>
            </div>
        </div>
        <div class="text-center text-white pt-5 pb-3 border-top border-secondary">© 2020 Copyright:
          <a href="<?php echo base_url('/'); ?>"> marpelis.dvt4-startupfp.es</a>
        </div>
      </div>
</div>
</footer> 
</html>