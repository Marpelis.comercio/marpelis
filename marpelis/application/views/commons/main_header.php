<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.css');?>" />
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Bevan" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>"/>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/boton-arriba.css');?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/DataTables-1.10.20/css/dataTables.bootstrap4.min.css');?>"/>
        <script type="text/javascript" src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/DataTables-1.10.20/js/jquery.dataTables.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/DataTables-1.10.20/js/dataTables.bootstrap4.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/tienda.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/boton-arriba.js');?>"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
        <title>Marpelis</title>
        <link  rel="icon" href="/assets/images/favicon.ico"/>
    </head>
    <body style="background-color: #212F3D;">
        <div class="container-fluid">
            <div class="row p-4 mx-0 border border-secondary" style="background-color: #1D2935">
                <div class="col-1">
                    <a href="<?php echo site_url('/');?>">
                     <img src="/assets/images/logo_blanco.png" class="img-responsive" style="width: 80%">
                    </a>
                </div>                    
                <div class="col-4">
                    <h1 class="text-white text-uppercase pl-4 mt-2" style="font-family: Bevan">
                     <?php echo $titulo;  ?>
                    </h1>
                </div>
                <div class="col-2">
                    <a href="https://www.marvel.com/movies" target="_blank">
                    <img src="/assets/images/partner1.JPG" class="img-responsive mt-2 ml-5" style="width: 95%">
                    </a>
                </div>
                <div class="col-2">
                    <a href="https://www.dccomics.com/movies" target="_blank">
                    <img src="/assets/images/partner2.png" class="img-responsive mt-2 ml-4" style="width: 22%">
                    </a>
                </div>
                <div class="col-3">                                 
                    <span class="float-right">
                        <a class="float-right ml-3 mt-3" href='<?php echo site_url('auth/logout');?>' >
                             <span class="fas fa-sign-out-alt fa-2x text-white float-right" aria-hidden="true"></span>
                        </a>
                        <h4 class="float-right mt-3 text-white"><?php echo $this->ion_auth->user()->row()->last_name,', '.$this->ion_auth->user()->row()->first_name; ?></h4>
                    </span> 
                </div>
            </div>
            <a href="javascript:void(0)" class="ir-arriba" title="Volver arriba">
                <span class="fa-stack">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-arrow-up fa-stack-1x fa-inverse"></i>
                </span>
            </a>
        