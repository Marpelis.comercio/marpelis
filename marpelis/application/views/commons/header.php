<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.css');?>" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/DataTables-1.10.20/css/dataTables.bootstrap4.min.css');?>"/>
        <script type="text/javascript" src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/DataTables-1.10.20/js/jquery.dataTables.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/DataTables-1.10.20/js/dataTables.bootstrap4.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/tienda.js');?>"></script>
        <title><?php echo $titulo; ?></title>
        <link  rel="icon" href="/assets/images/favicon.ico"/>
    </head>
    <body style="background-color: #212F3D;">
         <div class="container-fluid">
             <div class="row" style="background-color: #1D2935">
                <div class="col-8">
                    <h1 class="text-white col-12">
                        <?php echo $titulo; ?>
                    </h1>
                </div>
                <div class="col-4">
                    <span class="float-right">
                        <a class="float-right ml-3 mt-3" href='https://marpelis.dvt4.startupfp.es/auth/logout' >
                             <span class="fas fa-sign-out-alt fa-2x text-white float-right" aria-hidden="true"></span>
                        </a>
                        <h4 class="float-right mt-3 text-white"><?php echo $this->ion_auth->user()->row()->last_name,', '.$this->ion_auth->user()->row()->first_name; ?></h4>
                    </span> 
                </div>
            </div>
            <br>