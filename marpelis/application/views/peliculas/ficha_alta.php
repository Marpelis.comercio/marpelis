<?php echo validation_errors();?>
<div class="row text-white">
<?php echo $this->session->flashdata('exito');?>
</div>    

<?php echo form_open_multipart(site_url('articulo/alta'), ['class'=>'form-horizontal']);?>
<div class="row text-white">
    <div class="col-8">
        <div class="form-row">
            <div class="col-3">
                <?php echo form_label('Código:','codigo');?>
                <?php $marca1 = form_error('codigo')!== '' ? 'border-danger bg-warning':'';?>
                <?php echo form_input(['name'=>'codigo','id'=>'codigo', 'class'=>"form-control $marca1", 'value'=>set_value('codigo')]);?>
                <?php echo form_error('codigo','<div class="small text-danger">','</div>');?>
            </div>
            <div class="col-9">
                <?php echo form_label('Nombre:','nombre');?>
                <?php echo form_input(['name'=>'nombre','id'=>'nombre', 'class'=>'form-control','placeholder'=>'Introduce el nombre del artículo', 'value'=>set_value('nombre')]);?>
            </div>    
        </div>  
        <div class="form-row">
            <div class="col-3">
                <?php echo form_label('Duración:','duracion');?>
                <?php echo form_input(['name'=>'duracion','id'=>'duracion', 'class'=>'form-control', 'placeholder'=>'En minutos', 'value'=>set_value('duracion')]);?>
            </div>
            <div class="col-3">
                <?php echo form_label('Año:','año');?>
                <?php echo form_input(['name'=>'año','id'=>'año', 'class'=>'form-control', 'value'=>set_value('año')]);?>
            </div>
            <div class="col-3">
                <?php echo form_label('Género:','genero');?>
                <?php echo form_dropdown('genero', $generos, set_value('genero'), ['id'=>'genero','class'=>'form-control']);?>
            </div>
        </div>
        <div class="form-row">
            <div class="col-12">
                <?php echo form_label('Descripción:','descripcion');?>
                <?php echo form_input(['name'=>'descripcion','id'=>'descripcion', 'class'=>'form-control', 'value'=>set_value('descripcion')]);?>
            </div>   
        </div>
        <div class="form-row">
            <div class="col-12">
                <?php echo form_label('Youtube:','youtube');?>
                <?php echo form_input(['name'=>'youtube','id'=>'youtube', 'class'=>'form-control', 'placeholder'=>'Introduce el link de youtube', 'value'=>set_value('youtube')]);?>
            </div>   
        </div> 
    </div> 
    <div class="col-4">
        <div class="col-12">
            <div class="custom-file">
                <?php echo form_upload(['name'=>'imagen','id'=>'imagen','class'=>'custom-file-control','onchange'=>'readURL(this);', 'style'=>'display:none;']);?>
                <?php echo form_label('Selecciona la imagen jpg','imagen',['class'=>'custom-file-label']);?>
            </div>
        </div>
        <?php if (isset($error)): ?>
        <div class="col-12">
            <div class="bg-warning">
                <?php echo "Error: ".$error; ?>
            </div>
        </div>
        <?php endif; ?>
        <div class="col-12">
            <img id="visor" class=""  width="100%">
        </div>
        
    </div>    
</div>
<br>
<div class="form-row">
    <div class="col-12">
        <?php echo form_submit('enviar', 'Guardar');?>
    </div>   
</div>
<?php echo form_close(); ?>

