            <nav class="navbar sticky-top navbar-expand-xl navbar-light bg-white border-top border-dark rounded-bottom">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                  <ul class="navbar-nav">
                    <li class="nav-item text-uppercase">
                      <a class="nav-link" href="<?php echo site_url('/');?>">Inicio</a>
                    </li>
                    <li class="nav-item text-uppercase">
                      <a class="nav-link" href="<?php echo site_url('articulo/generos');?>">Géneros</a>
                    </li>
                  </ul>
                </div>
              </nav>
<div class="row">
                <div class="col-4"></div>
                <div id="carouselExampleSlidesOnly" class="col-8 carousel slide mt-3" data-ride="carousel">
                  <div class="carousel-inner">
                    <div class="carousel-item">
                      <img src="<?php echo base_url('assets/images/anuncio1.jpg'); ?>" class="d-block w-50" alt="Disfraces">
                    </div>
                    <div class="carousel-item">
                      <img src="<?php echo base_url('assets/images/anuncio2.jpg'); ?>" class="d-block w-50" alt="Videojuego">
                    </div>
                    <div class="carousel-item active">
                      <img src="<?php echo base_url('assets/images/anuncio3.jpg'); ?>" class="d-block w-50" alt="Tazas">
                    </div>
                  </div>
                </div>                
            </div>
            <div class="text-white p-0 mt-3 float-left border border-light rounded" style="width: 20%; background: #1D2935" >
                <h3 class="text-center m-3"><?php echo $resultado->nombre; ?></h3>             
                <div class="text-center m-3">
                    <img src="<?php echo base_url('assets/images/articles/'.$resultado->codigo.'.jpg');?>" style="width: 70%">
                </div>
                <div  class="text-center m-3">
                    <p>Año: <?php echo $resultado->año; ?></p>
                    <p>Duración: <?php echo $resultado->duracion; ?> min</p>
                    <p>Género: <?php echo $genero->nombre;?></p>
                </div>
            </div>              
            <div class="float-right mt-3 border border-light rounded" style="width: 77%; background: #1D2935">
                <div class="m-3 text-white">
                    <h3 class="text-center text-uppercase">Sinopsis</h3>
                    <p><?php echo $resultado->descripcion; ?></p>
                </div>
                <h3 class="text-white text-center">Tráiler</h3>
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item mt-4" style="width: 80%; height: 80%; left: 10% " src="<?php echo $resultado->youtube; ?>" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </body>
</html>