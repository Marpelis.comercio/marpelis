            <nav class="navbar sticky-top navbar-expand-xl navbar-light bg-white border-top border-dark rounded-bottom">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                  <ul class="navbar-nav">
                    <li class="nav-item active text-uppercase">
                      <a class="nav-link" href="">Inicio<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item text-uppercase">
                      <a class="nav-link" href="<?php echo site_url('articulo/generos');?>">Géneros</a>
                    </li>
                  </ul>
                </div>
              </nav>           
            <div class="row">
                <div class="col-4"></div>
                <div id="carouselExampleSlidesOnly" class="col-8 carousel slide mt-3" data-ride="carousel">
                  <div class="carousel-inner">
                    <div class="carousel-item">
                      <img src="<?php echo base_url('assets/images/anuncio1.jpg'); ?>" class="d-block w-50" alt="Disfraces">
                    </div>
                    <div class="carousel-item">
                      <img src="<?php echo base_url('assets/images/anuncio2.jpg'); ?>" class="d-block w-50" alt="Videojuego">
                    </div>
                    <div class="carousel-item active">
                      <img src="<?php echo base_url('assets/images/anuncio3.jpg'); ?>" class="d-block w-50" alt="Tazas">
                    </div>
                  </div>
                </div>                
            </div>
            <div class="card-group mt-3">  
                <?php foreach ($resultado as $pelicula): ?>
                    <div class="col-sm-4">
                        <div class="card m-3">
                            <img src="<?php echo base_url('assets/images/articles/'.$pelicula->codigo.'.jpg');?>" class="card-img-top" alt="<?php echo $pelicula->nombre; ?>">
                            <strong  class="text-center text-uppercase card-title text-white" style="background: #1D2935"><?php echo $pelicula->nombre; ?></strong>
                            <p class="card-body">
                                <?php echo substr($pelicula->descripcion,0,100)." ..."; ?>

                                <span style="color:blue"> ( <?php echo $pelicula->duracion; ?> min )</span>
                            </p>
                            <p class="text-center">
                                <a class="btn btn-dark" href="<?php echo base_url('articulo/pelicula/'. $pelicula->id) ?>" role="button">Ver Película</a>
                            </p>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>    
        </div>    
    </body>
   
