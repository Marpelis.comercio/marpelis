            <nav class="navbar sticky-top navbar-expand-xl navbar-light bg-white border-top border-dark rounded-bottom">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                  <ul class="navbar-nav">
                    </li>
                    <li class="nav-item text-uppercase">
                      <a class="nav-link" href="<?php echo site_url('/');?>">Inicio</a>
                    </li>
                    <li class="nav-item active text-uppercase">
                      <a class="nav-link" href="">Géneros<span class="sr-only">(current)</span></a>
                    </li>
                  </ul>
                </div>
              </nav>
            <div class="row">
                <div class="col-4"></div>
                <div id="carouselExampleSlidesOnly" class="col-8 carousel slide mt-3" data-ride="carousel">
                  <div class="carousel-inner">
                    <div class="carousel-item">
                      <img src="<?php echo base_url('assets/images/anuncio1.jpg'); ?>" class="d-block w-50" alt="Disfraces">
                    </div>
                    <div class="carousel-item">
                      <img src="<?php echo base_url('assets/images/anuncio2.jpg'); ?>" class="d-block w-50" alt="Videojuego">
                    </div>
                    <div class="carousel-item active">
                      <img src="<?php echo base_url('assets/images/anuncio3.jpg'); ?>" class="d-block w-50" alt="Tazas">
                    </div>
                  </div>
                </div>                
            </div>
            
           
            <div class="container">
                <?php echo $secciones ?>
            </div>
