
<?php echo form_open(site_url('articulo/edita/'.$id), ['class'=>'form-horizontal']);?>
    <div class="form-row text-white">
        <div class="col-2">
            <?php echo form_label('Código:','codigo');?>
            <?php $marca = form_error('codigo')!== '' ? 'border-danger bg-warning':'';?>
            <?php echo form_input(['name'=>'codigo','id'=>'codigo', 'class'=>"form-control $marca", 'value'=>set_value('codigo',$pelicula->codigo)]);?>
            <?php echo form_error('codigo','<div class="small text-danger">','</div>');?>
        </div>
        <div class="col-6">
            <?php echo form_label('Nombre:','nombre');?>
            <?php echo form_input(['name'=>'nombre','id'=>'nombre', 'class'=>'form-control','placeholder'=>'Introduce el nombre del artículo', 'value'=>set_value('nombre',$pelicula->nombre)]);?>
        </div>    
    </div>  
    <div class="form-row text-white">
        <div class="col-2">
            <?php echo form_label('Duración:','duracion');?>
            <?php echo form_input(['name'=>'duracion','id'=>'duracion', 'class'=>'form-control', 'value'=>set_value('duracion',$pelicula->duracion)]);?>
        </div>
        <div class="col-2">
            <?php echo form_label('Año:','año');?>
            <?php echo form_input(['name'=>'año','id'=>'año', 'class'=>'form-control', 'value'=>set_value('año',$pelicula->año)]);?>
        </div>
        <div class="col-2">
            <?php echo form_label('Género:','genero');?>
            <?php echo form_dropdown('genero', $generos, set_value('genero',$pelicula->genero), ['id'=>'genero','class'=>'form-control']);?>
        </div>
    </div>
    <div class="form-row text-white">
        <div class="col-8">
            <?php echo form_label('Descripción:','descripcion');?>
            <?php echo form_input(['name'=>'descripcion','id'=>'descripcion', 'class'=>'form-control', 'value'=>set_value('descripcion',$pelicula->descripcion)]);?>
        </div>   
    </div>
    <div class="form-row text-white">
        <div class="col-8">
            <?php echo form_label('Youtube:','youtube');?>
            <?php echo form_input(['name'=>'youtube','id'=>'youtube', 'class'=>'form-control', 'placeholder'=>'Introduce el link de youtube', 'value'=>set_value('youtube',$pelicula->youtube)]);?>
        </div>   
    </div>
    <br>
    <div class="form-row ">
        <div class="col-8">
            <?php echo form_submit('enviar', 'Guardar');?>
        </div>   
    </div>
<?php echo form_close();?>

