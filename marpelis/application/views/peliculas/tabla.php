<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row">
    <div class="offset-5 col-2">
        <a href="<?php echo site_url('articulo/alta');?>" class="btn btn-outline-info">
            Nueva Película <span class="fas fa-plus-circle"></span>
        </a>
    </div>
</div>
<br>
            <div class="row d-flex justify-content-center text-white">  
                <table class="table table-striped table-condensed dataTable text-white">
                    <thead>
                        <th class="col-md-1"></th>                       
                        <th class="col-md-5">Película</th>                        
                        <th class="col-md-1">Duración</th>
                        <th class="col-md-1">Género</th>
                        <th class="col-md-5">Trailer</th>
                        <th></th>
                    </thead>
                    <tbody>
                    <?php foreach ($resultado as $pelicula): ?>
                        <tr>
                            <td>
                                <img width="40px" src="<?php echo base_url('assets/images/articles/'.$pelicula->codigo.'.jpg');?>" alt="<?php echo $pelicula->nombre; ?>"> 
                            </td>
                            <td>
                               <?php echo $pelicula->nombre; ?>
                            </td>
                            <td>
                               <?php echo $pelicula->duracion; ?>
                            </td>
                            <td>
                               <?php echo $pelicula->genero; ?>
                            </td>
                            <td>
                               <?php echo $pelicula->youtube; ?>
                            </td>
                            <td>
                                <a href="<?php echo site_url('articulo/edita/' . $pelicula->id); ?>" class="btn btn-sm btn-outline-info" title="Editar un registro" >
                                    <span class="fas fa-edit"></span>
                                </a>
                                <a href="<?php echo site_url('articulo/borra/' . $pelicula->id); ?>" class="btn btn-sm btn-outline-danger"  title="Borra un registro" onclick="return confirm('¿Estás seguro de borrar el artículo <?php echo $pelicula->nombre; ?>?')">
                                    <span class="fas fa-trash"></span>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>    
                </table>
            </div>        
 

