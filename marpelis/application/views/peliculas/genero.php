<div class="row">
    <div class="col-12 p-2">
        <div class="rounded bg-white" style="width: 20%;">
        <h2 class="text-alert text-center">
            <?= $nombre_genero?>
        </h2>
      </div>
    </div>  
    <?php foreach($peliculas as $pelicula):?>
    <div class="card m-2" style="width: 200px;">
        <img src="<?php echo base_url('assets/images/articles/'.$pelicula->codigo.'.jpg');?>" class="card-img-top" alt="<?php echo $pelicula->nombre; ?>" width="100px">
        <small class="text-center text-uppercase card-title text-white" style="background: #1D2935"><?php echo $pelicula->nombre; ?></small>
    <div class="card-body">
        <p class="text-center">
          <a class="btn btn-dark" href="<?php echo base_url('articulo/pelicula/'. $pelicula->id) ?>" role="button">Ver Película</a>
        </p>
    </div>
    </div>
    <?php endforeach; ?>
</div>

