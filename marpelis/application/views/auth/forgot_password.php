<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/fontawesome/css/all.css');?>" />
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Bevan" />
        <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/DataTables-1.10.20/css/dataTables.bootstrap4.min.css');?>"/>
        <script type="text/javascript" src="<?php echo base_url('assets/jQuery-3.3.1/jquery-3.3.1.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/DataTables-1.10.20/js/jquery.dataTables.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/DataTables-1.10.20/js/dataTables.bootstrap4.min.js');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/tienda.js');?>"></script>
        <title>Inicio de Sesión peliculas Marpelis</title>
        <link  rel="icon" href="/assets/images/favicon.ico"/>
    </head>
    <body background="/assets/images/3.jpg">
        <div class="container-fluid">
            <div class="row" style="background-color: #212F3D;">
                <div class="col-1">
                    <img src="/assets/images/logo_blanco.png" class="img-responsive m-1 ml-5" style="width: 65%">
                </div>
                <div class="col-3">
                    <h1 class="text-white text-uppercase pl-4 mt-2" style="font-family: Bevan">
                    Marpelis
                    </h1>
                </div>
                <div class="col-2">
                    <a href="https://www.marvel.com/movies" target="_blank">
                    <img src="/assets/images/partner1.JPG" class="img-responsive mt-2 ml-5" style="width: 95%">
                    </a>
                </div>
                <div class="col-2">
                    <a href="https://www.dccomics.com/movies" target="_blank">
                    <img src="/assets/images/partner2.png" class="img-responsive mt-2 ml-4" style="width: 22%">
                    </a>
                </div>
                <div class="col-4">
                    <a class="float-right m-2 p-1 btn btn-light" href="https://gitlab.com/Marpelis.comercio/marpelis" role="button" target="_blank">
                        <span class="fab fa-gitlab fa-2x" style="color: #D35400" aria-hidden="true"></span>
                    </a> 
                    <a class="float-right m-2 p-1 btn btn-light" href="https://twitter.com/Marpelis1" role="button" target="_blank">
                        <span class="fab fa-twitter fa-2x" style="color: #00acee" aria-hidden="true"></span>
                    </a>                                      
                    <a class="float-right m-2 p-1 btn btn-light" href="https://www.facebook.com/profile.php?id=100051649306287" role="button" target="_blank">
                        <span class="fab fa-facebook-square fa-2x" style="color: #3b5998" aria-hidden="true"></span>
                    </a>                                     
                    <a class="float-right m-2 p-1 btn btn-light" href="https://www.instagram.com/marpelis.comercio/" role="button" target="_blank">
                        <span class="fab fa-instagram fa-2x" style="color: #C13584" aria-hidden="true"></span>
                    </a>                    
                </div>
            </div>
            <div class="row">
                <div class="col-3"></div>
                <div class="col-9">    
                  <div class="text-center text-white m-5 border border-light" style="width: 50%;background-color: #212F3D;">
                    <h1 class="mt-3" style="font-family: Bevan;"><?php echo lang('forgot_password_heading');?></h1>
                    <p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>

                    <div id="infoMessage"><?php echo $message;?></div>

                    <?php echo form_open("auth/forgot_password");?>

                          <p>
                            <label for="identity"><?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label> <br />
                            <?php echo form_input($identity);?>
                          </p>

                          <p><?php echo form_submit('submit', lang('forgot_password_submit_btn'));?></p>

                    <?php echo form_close();?>
            </div>
          </div>
         </div>


