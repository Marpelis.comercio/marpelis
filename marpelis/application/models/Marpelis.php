<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Marpelis
 *
 * @author Marcos
 */
class Marpelis extends CI_Model{
    
    public function get_articulos() {
//El primer paso es escribir la consulta y guardarla en la variable $sql
        $sql = <<< SQL
            SELECT * 
              FROM peliculas
              Where borrado = 0
SQL;
//Ejecutar la consulta
        $consulta = $this->db->query($sql);
//Pasar los resultados al controlador
        return $consulta->result();
    }
    public function get_pelicula($id) {
//El primer paso es escribir la consulta y guardarla en la variable $sql
        $sql = <<< SQL
            SELECT * 
              FROM peliculas
              WHERE id = ?
SQL;
//Ejecutar la consulta
        $consulta = $this->db->query($sql,[$id]);
//sólo el primero de un array de 1        
        return $consulta->result()[0];
    }
    public function add_pelicula($pelicula){
        //insertamos en la tabla articulos el array donde las claves son los nombres
        //de los campos
        $this->db->insert('peliculas',$pelicula);
    }
    
    public function  guardar($pelicula){
        $this->db->insert('peliculas',$pelicula);
    }
    
    public function actualizar($pelicula,$id){
        $this->db->update('peliculas',$pelicula,['id'=>$id]);
    }

    
    public function get_generos(){
        $sql = <<< SQL
            SELECT *
             FROM generos
SQL;
        $consulta = $this->db->query($sql);
        $array = [];
        foreach ($consulta->result() as $item){
            $array[$item->id] = $item->nombre;
        }
        return $array;
    }
    public function get_genero($id = NULL){
        if($id != NULL){
            $this->db->where(['id'=>$id]);
        }
        $consulta = $this->db->get('generos');
        if($id == NULL){
            return $consulta->result();
        }else{
            return $consulta->result()[0];
        }   
    }
    public function get_pelicula_genero($genero){
        $this->db->where(['genero'=>$genero]);
        $consulta = $this->db->get('peliculas');
        return $consulta->result();
    }   
    
     public function delete($id){
        $this->db->where(['id'=>$id]);
        $this->db->update('peliculas',['borrado'=>1]);
    }
    
}